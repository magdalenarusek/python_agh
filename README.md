**Python Tasks - AGH**

**Zad 1** : INPUT/OUTPUT

- _zad1aa.py_ - Hello World with shebang
- _zad1ab.py_ - Hello World without shebang
- _zad1b.py_ - Data Input
- _zad1c.py_ - Data Save

**Zad 2** : WORKING WITH FILES
- _zad2a.py_ - Number of files
- _zad2b.py_ - Directory structure
- _zad2c.py_ - Converting an extension

**Zad 3** : TEXT
- _zad3a.py_ - Deleting words
- _zad3b.py_ - Replacing words

**Zad 4** : CALCULATIONS AND ALGORITHMS
- _zad4a.py_ - Quadratic equation
- _zad4b.py_ - Sorting
- _zad4c.py_ - Scalar product
- _zad4d.py_ - Sum of matrices
- _zad4e.py_ - Mnożenie macierzy
- _zad4f.py_ - Determinant of the matrix

**Zad 5** : CLASSES
- _zad5a.py_ - Complex numbers
- _zad5b.py_ - Calculator

**Zad 6** : WORKING WITH DATA
- _zad6aa.py_ - XML - SAX
- _zad6ab.py_ - XML - DOM
- _zad6b.py_ - CSV

**Zad 7** : PARALLELIZATION OF CALCULATIONS
- _zad7a.py_ - Histogram
- _zad7b.py_ - Five philosophers
